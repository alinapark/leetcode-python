"""
Given an integer n, find the closest integer (not including itself), which is a palindrome.

The 'closest' is defined as absolute difference minimized between two integers.
"""
from math import log10
class Solution:
    def nearestPalindromic(self, n):
        """
        :type n: str
        :rtype: str
        """
        if len(n) == 1: return str(int(n)-1)
        elif log10(int(n)).is_integer() or log10(int(n)-1).is_integer(): return (len(n) - 1) * "9"
        elif log10(int(n)+1).is_integer(): return "1" + "0"*(len(n) - 1) + "1"

        mid_pos = [int(len(n)/2)] if len(n) % 2 == 1 else [int(len(n)/2)-1, int(len(n)/2)]
        
        reflected = [int(n[i]) if i < int(len(n)/2) else int(n[len(n)-1-i]) for i in range(len(n))]
        upper = int(''.join(map(str,[reflected[i] if i not in mid_pos else (reflected[i]+1)%10 for i in range(len(n))])))
        lower = int(''.join(map(str,[reflected[i] if i not in mid_pos else (reflected[i]-1)%10 for i in range(len(n))])))
        reflected, n = int(''.join(map(str,reflected))) if n != n[::-1] else lower, int(n)
          
        return str(min(lower,reflected,upper, key=lambda num: abs(n-num)))      