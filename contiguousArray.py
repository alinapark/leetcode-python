"""
Given a binary array, find the maximum length of a contiguous subarray with equal number of 0 and 1.
"""
class Solution(object):
    def findMaxLength(self, nums):
        numb = 0
        maxv=0
        table = {0: 0}
        for index, num in enumerate(nums, 1):
            if num == 0:
                numb -= 1
            else:
                numb += 1
            
            if numb in table:
                maxv = max(maxv, index - table[numb])
            else:
                table[numb] = index
        
        return maxv